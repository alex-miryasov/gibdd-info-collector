const webdriver = require('selenium-webdriver');
const { By, until, Key } = webdriver;

const solver = require('2captcha');
const { Sleep } = require('./helpers');
const { RandomNumber } = require('./helpers');
const { ProxyGroup } = require('./Proxies');
const { default: PQueue } = require('p-queue');

const Proxy1Queue = new PQueue({ concurrency: 1 });
const Proxy2Queue = new PQueue({ concurrency: 1 });
const Proxy3Queue = new PQueue({ concurrency: 1 });
const Proxy4Queue = new PQueue({ concurrency: 1 });
const Proxy5Queue = new PQueue({ concurrency: 1 });
const Proxy6Queue = new PQueue({ concurrency: 1 });
const Proxy7Queue = new PQueue({ concurrency: 1 });
const Proxy8Queue = new PQueue({ concurrency: 1 });
const Proxy9Queue = new PQueue({ concurrency: 1 });
const Proxy10Queue = new PQueue({ concurrency: 1 });

async function GetErrorVIN (vinTaskDB) {
  let idsBox = await vinTaskDB.get(`idsBox`);
  let DBData = [];
  for (let i = 0; i < idsBox.length; i++) {
    await DBData.push(await vinTaskDB.get(`task${idsBox[i]}`));
  }
  await console.log('IDS BOX LENGTH: ', DBData.length);
  let ReturnErrorVINs = [];
  for (let kk = 0; kk < DBData.length; kk++) {
    try {
      if (!!await DBData[kk].status) {
        if (await DBData[kk].status === 'error') {
          await ReturnErrorVINs.push(await DBData[kk].vin, await DBData[kk].checkDate);
        }
      }
    } catch (e) {
      await console.log('!STATUS');
    }
  }
  return await ReturnErrorVINs;
}

async function GetProxyData () {
  let randomProxy = await RandomNumber(0, ProxyGroup.length - 1);

  let address = await ProxyGroup[randomProxy].address;
  let login = await ProxyGroup[randomProxy].login;
  let password = await ProxyGroup[randomProxy].password;

  return { address, login, password, queue: randomProxy };
}

async function GetProxyQueue () {
  for (let ii = 0; ii < 5; ii++) {
    switch (ii) {
      case 0:
        if (!await Proxy1Queue.pending) {
          await console.log('RETURNING PROXY QUEUE: 1');
          return await Proxy1Queue;
        }
      case 1:
        if (!await Proxy2Queue.pending) {
          await console.log('RETURNING PROXY QUEUE: 2');
          return await Proxy2Queue;
        }
      case 2:
        if (!await Proxy3Queue.pending) {
          await console.log('RETURNING PROXY QUEUE: 3');
          return await Proxy3Queue;
        }
      case 3:
        if (!await Proxy3Queue.pending) {
          await console.log('RETURNING PROXY QUEUE: 4');
          return await Proxy4Queue;
        }
      case 4:
        if (!await Proxy3Queue.pending) {
          await console.log('RETURNING PROXY QUEUE: 5');
          return await Proxy5Queue;
        }
      case 5:
        if (!await Proxy3Queue.pending) {
          await console.log('RETURNING PROXY QUEUE: 6');
          return await Proxy6Queue;
        }
      case 6:
        if (!await Proxy3Queue.pending) {
          await console.log('RETURNING PROXY QUEUE: 7');
          return await Proxy7Queue;
        }
      case 7:
        if (!await Proxy3Queue.pending) {
          await console.log('RETURNING PROXY QUEUE - 8');
          return await Proxy8Queue;
        }
      case 8:
        if (!await Proxy3Queue.pending) {
          await console.log('RETURNING PROXY QUEUE: 9');
          return await Proxy9Queue;
        }
      case 9:
        if (!await Proxy3Queue.pending) {
          await console.log('RETURNING PROXY QUEUE: 10');
          return await Proxy10Queue;
        }
    }
  }
}

async function GetTextFromCaptcha (driver, imageXpath) {
  let returningCaptchaText;
  let img = await driver.findElement(By.xpath(imageXpath));
  await img.takeScreenshot().then(async function (image, err) {
    await require('fs').writeFile('captcha.png', image, 'base64', async () => {
      if (!!err) {
        await console.log('TAKING SCREEN-SHOT ERROR: ', await err);
      }
      await console.log('SCREEN-SHOT IMAGE SAVED');
      await solver.decode(await image.toString('base64'), {}, async (decodeErr, result, invalid) => {
        await console.log('DECODED');
        if (await decodeErr) {
          await console.log('SOLVER ERROR: ', await decodeErr);
          return;
        }
        if (!!await result) {
          await console.log('SOLVER DECODE: ', await result.text);
          returningCaptchaText = await result.text;
        }
      });
    });
  });
  do {
    await console.log('WAITING');
    await Sleep(1500);
  } while (!returningCaptchaText);
  await console.log('DONE');
  return await returningCaptchaText;
}

async function CalculateDate (date1, date2) {
  if (date1.includes('.')) {
    let splittedDate1 = await date1.split('.');
    let splittedDate2 = await date2.split('.');

    let parsedDate1 = await ParseDateToNumber(await splittedDate1);
    let parsedDate2 = await ParseDateToNumber(await splittedDate2);

    return await DateMinusDate(new Date(await parsedDate2[2], await parsedDate2[1], await parsedDate2[0]), await parsedDate1[0], await parsedDate1[1], await parsedDate1[2]);
  }
}

async function ParseDateToNumber (array) {
  let parsedArray = [];
  for (let ii = 0; ii < array.length; ii++) {
    await parsedArray.push(await parseInt(await array[ii]));
  }
  return parsedArray;
}

async function DateMinusDate (date, days, month, years) {
  let dateMessage;

  let y = await date.getFullYear();
  let m = await date.getMonth();
  let d = await date.getDate();

  let firstDateString = await d + '.' + await m + '.' + await y;
  let secondDateString = await days + '.' + await month + '.' + await years;
  if (firstDateString === secondDateString) {
    dateMessage = 'ПЕРЕРЫВА НЕ БЫЛО';
    await console.log(dateMessage);
  } else {
    await console.log('ПЕРЕРЫВ БЫЛ');
    await console.log('FIRST DATE: ' + firstDateString);
    await console.log('SECOND DATE: ' + secondDateString);
    let reworkedDate = new Date(await y - await years, await m - await month, await d - await days);
    let newYear = await reworkedDate.getFullYear() - 1900;

    dateMessage = (newYear > 0)
      ? 'ПЕРЕРЫВ В: ' + await newYear.toString() + ' ЛЕТ, ' + await reworkedDate.getMonth().toString() + ' МЕСЯЦЕВ, ' + await reworkedDate.getDate().toString() + ' ДНЕЙ.'
      : 'ПЕРЕРЫВ В: ' + await reworkedDate.getMonth().toString() + ' МЕСЯЦЕВ, ' + await reworkedDate.getDate().toString() + ' ДНЕЙ.';

    await console.log(dateMessage);
  }
  return dateMessage;
}

module.exports.GetErrorVIN = GetErrorVIN;
module.exports.GetProxyData = GetProxyData;
module.exports.GetProxyQueue = GetProxyQueue;
module.exports.GetTextFromCaptcha = GetTextFromCaptcha;
module.exports.CalculateDate = CalculateDate;
