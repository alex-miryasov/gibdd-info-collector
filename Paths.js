const CaptchaElementXpath = `//*[contains(@class, 'grecaptcha-badge')]`;

const VINFieldXpath = `//*[contains(@id, 'checkAutoVIN')]`;

const CheckHistoryXpath = `//*[contains(@id, 'checkAutoHistory')]//*[contains(@class, 'checker')]`;
const WaitWhileCheckingHistoryXpath = `//*[contains(@id, 'checkAutoHistory')]//*[contains(@class, 'check-message')]//*[contains(text(), 'Выполняется запрос, ждите...')]`;
const CheckingHistoryResultXpath = `//*[contains(@id, 'checkAutoHistory')]//*[contains(@class, 'checkResult')]`;
const CheckingHistoryResultNotFoundXpath = `//*[contains(@id, 'checkAutoHistory')]//*[contains(text(), 'В результате обработки запроса')]`;
const CarDataListXpath = `//*[contains(@id, 'checkAutoHistory')]//*[contains(@class, 'checkResult')]//*[contains(@class,'field vehicle')]`;
const CheckingHistoryErrorXpath = `//*[contains(@id, 'checkAutoHistory')]//*[contains(@class, 'check-message')][contains(text(), 'Проверка завершилась ошибкой на стороне сервера')]`;
const OwnershipPeriodFromXpath = `//*[contains(@id, 'checkAutoHistory')]//*[contains(@class, 'ownershipPeriods-from')]`;
const OwnershipPeriodToXpath = `//*[contains(@id, 'checkAutoHistory')]//*[contains(@class, 'ownershipPeriods-to')]`;
const CarLastActionXpath = `//*[contains(@id, 'checkAutoHistory')]//*[contains(@class, 'checkResult')]//*[contains(text(),'Последняя операция')]`;
const DateOfInfoChecking = `//*[contains(@id, 'checkAutoHistory')]//*[contains(text(), 'Проверка проведена')]`;

// const OwnershipPeriodXpath = `//*[contains(@id, 'checkAutoHistory')]//*[@class = 'ownershipPeriods']`;
// const MainMenuServicesXpath = `//*[contains(@id, 'mobile-menu')]//*[contains(text(), 'Сервисы')]`;
// const CarSearchingBTNXpath = `//*[contains(@class, 'bn-top-submenu')]//*[contains(text(), 'Проверка автомобиля')]`;

const CheckDTPHistoryBTNXpath = `//*[contains(@id, 'checkAutoAiusdtp')]//*[contains(@class, 'checker')]`;
const WaitWhileCheckingDTPXpath = `//*[contains(@id, 'checkAutoAiusdtp')]//*[contains(@class, 'check-message')]//*[contains(text(), 'Выполняется запрос, ждите...')]`;
const CheckingDTPErrorXpath = `//*[contains(@id, 'checkAutoAiusdtp')]//*[contains(@class, 'check-message')][contains(text(), 'Проверка завершилась ошибкой на стороне сервера')]`;
const CheckingDTPResultXpath = `//*[contains(@id, 'checkAutoAiusdtp')]//*[contains(@class, 'checkResult')]`;
const CheckingDTPResultNotFoundXpath = `//*[contains(@id, 'checkAutoAiusdtp')]//*[contains(text(), 'В результате обработки запроса')]`;
const GetDTPInfoNumberXpath = `//*[contains(@id, 'checkAutoAiusdtp')]//*[contains(@class, 'checkResult')]//*[contains(@class,'ul-title')]`;
const GetDTPDataXpath = text => `//*[contains(@id, 'checkAutoAiusdtp')]//*[contains(@class, 'checkResult')]//*[contains(text(), '${text}')]/parent::*//*[@class='field']`;
const ScreenDTPMapXpath = text => `//*[contains(@id, 'checkAutoAiusdtp')]//*[contains(@class, 'checkResult')]//*[contains(text(), '${text}')]/parent::*//*[contains(@id, 'damageTabNum')]`;
const DetailDTPInfoXpath = `//*[contains(@id, 'checkAutoAiusdtp')]//*[contains(@class, 'checkResult')]//*[contains(text(), 'Информация о происшествии')]/parent::*/parent::*/parent::*//*/td[2][@valign='top']/text()[last()]`;
const MapImagePath = (text) => `//*[contains(@id, 'checkAutoAiusdtp')]//*[contains(@class, 'checkResult')]//*[contains(text(), '${text}')]/parent::*//*[contains(@id, 'damageTabNum')]`;
const DateOfDTPChecking = `//*[contains(@id, 'checkAutoAiusdtp')]//*[contains(text(), 'Проверка проведена')]`;

const CheckWantedStatusBTNXpath = `//*[contains(@id, 'checkAutoWanted')]//*[contains(@class, 'checker')]`;
const WaitWhileCheckingWantedXpath = `//*[contains(@id, 'checkAutoWanted')]//*[contains(@class, 'check-message')]//*[contains(text(), 'Выполняется запрос, ждите...')]`;
const CheckingWantedErrorXpath = `//*[contains(@id, 'checkAutoWanted')]//*[contains(@class, 'check-message')][contains(text(), 'Проверка завершилась ошибкой на стороне сервера')]`;
const CheckingWantedResultXpath = `//*[contains(@id, 'checkAutoWanted')]//*[contains(@class, 'checkResult')]`;
const CheckingWantedResultNotFoundXpath = `//*[contains(@id, 'checkAutoWanted')]//*[contains(text(), 'В результате обработки запроса')]`;
const DateOfWantedChecking = `//*[contains(@id, 'checkAutoWanted')]//*[contains(text(), 'Проверка проведена')]`;

const CheckRestrictedStatusBTNXpath = `//*[contains(@id, 'checkAutoRestricted')]//*[contains(@class, 'checker')]`;
const WaitWhileCheckingRestrictionsXpath = `//*[contains(@id, 'checkAutoRestricted')]//*[contains(@class, 'check-message')]//*[contains(text(), 'Выполняется запрос, ждите...')]`;
const CheckingRestrictionsErrorXpath = `//*[contains(@id, 'checkAutoRestricted')]//*[contains(@class, 'check-message')][contains(text(), 'Проверка завершилась ошибкой на стороне сервера')]`;
const CheckingRestrictionsResultXpath = `//*[contains(@id, 'checkAutoRestricted')]//*[contains(@class, 'checkResult')]`;
const CheckingRestrictionsResultNotFoundXpath = `//*[contains(@id, 'checkAutoRestricted')]//*[contains(text(), 'В результате обработки запроса')]`;
const DateOfRestrictedChecking = `//*[contains(@id, 'checkAutoRestricted')]//*[contains(text(), 'Проверка проведена')]`;

const AutoExtraDamageListXpath = `//*[contains(@id, 'checkAutoAiusdtp')]//*[contains(@class, 'checkResult')]//td[@valign='top'][2]/text()[last()]`;
const AutoFullDamageListXpath = `//*[contains(@id, 'checkAutoAiusdtp')]//*[contains(@class, 'checkResult')]//*[contains(text(), 'Информация о происшествии')]/parent::*/parent::*/parent::*//*/td[2][@valign='top']/text()[last()]`;
const RestrictedListXpath = `//*[contains(@id, 'checkAutoRestricted')]//*[contains(@class, 'checkResult')]//*[contains(@class, 'fields-list restrict-data')]`;
const RestrictedItemDataXpath = (number) => `//*[contains(@id, 'checkAutoRestricted')]//*[contains(@class, 'checkResult')]//*[contains(@class, 'restricted-list')]/li[${number}]//*[contains(@class, 'fields-list restrict-data')]//*[contains(@class, 'field')]`;
const RestrictedItemLinkXpath = (number) => `//*[contains(@class, 'restricted-list')]/li[${number}]//*[contains(@title,'Проверить на сайте ФССП России')]`;
const DTPMapDescriptionXpath = `//*[contains(@id, 'checkAutoAiusdtp')]//*[contains(@class, 'checkResult')]/table`;

const GoogleReCaptchaNotCompleteXpath = `//*[contains(@class, 'check-space')][contains(text(), 'Google reCaptcha v3 не была пройдена')]`;

const RosStandardVINFieldXpath = `//*[contains(@id,'findVin')]`;
const RosStandardFindButtonXpath = `//*[contains(@id,'btnFindVin')]`;
const RosInfoAboutVINNotFoundXpath = `//*[contains(@id,'divDindVin')]//*[contains(text(), 'VIN')][contains(text(), 'не найден среди отзывных кампаний')]`;
const RosOnlyLatinsXpath = `//*[contains(text(), 'В VIN разрешено использовать только буквы латинского алфавита')]`;

const CustomsVINFieldXpath = `//*[contains(@name,'VEHICLE_VIN')]`;
const CustomsFindButtonXpath = `//*[contains(@type,'button')][contains(@onclick, 'search')]`;
const CustomsTableResultXpath = `//*[contains(@id,'search_result')]//*[contains(@class, 'even' | 'odd')]/td`;

const MosAuthLoginFieldXpath = `//*[contains(@id, 'login')][contains(@name, 'login')]`;
const MosAuthPasswordFieldXpath = `//*[contains(@id, 'password')][contains(@name, 'password')]`;
const MosAuthSubmitButtonXpath = `//*[contains(@id, 'bind')][contains(@type, 'button')]`;
const MosVINorGosNumberFieldXpath = `//*[contains(@class, 'group')]//*[contains(text(),'VIN')]/parent::*/input`;
const MosSRTSFieldXpath = `//*[contains(@class, 'group')]//*[contains(text(),'СТС')]/parent::*/input`;
const MosSubmitButtonXpath = `//*[contains(@class, 'autohistory-search__submit')]`;
const MosCaptchaImageXpath = `//*[@class='captcha-img']`;
const MosCaptchaFieldXpath = `//*[contains(@class, 'group')]//*[contains(text(),'Символы')]/parent::*/input`;
const MosCaptchaRefreshXpath = `//*[@class='refresh-captcha']`;
const MosVINorGosNumberErrorXpath = `//*[contains(text(), 'Неверный VIN или госзнак')]`;
const MosPleaseEnterToSiteXpath = `//*[contains(@class, 'link')][contains(text(), 'Войдите')]`;
const MosResponseNotFoundXpath = `//*[contains(text(), 'По вашему запросу ничего не найдено')]`;
const MosCarPriceXpath = `//*[contains(@class, 'carprice-price-desktop')]//*[@class='carprice-price']`;
const MosCarMileageXpath = `//*[contains(@class, 'main-info-container')]//*[contains(text(),'Пробег')]`;
const MosCarMileageInfoXpath = `//*[contains(text(),'Сведения о пробеге')]/parent::*/parent::*//*[contains(@class,'autohistory-table__td')]`;
const MosCarTechnicalInspectionXpath = `//*[contains(text(),'Технический осмотр')]/parent::*/parent::*//*[contains(@class,'autohistory-table__td')]`;

const NotarySearchInfoXpath = `//*[contains(text(), 'По информации о предмете залога')]`;
const NotaryINFieldXpath = `//*[contains(@id, 'vehicleProperty.vin')]`;
const NotarySubmitButtonXpath = `//*[contains(@id, 'find-btn')]`;
const NotaryCaptchaImageXpath = `//*[contains(@class, 'swal2-image')]`;
const NotaryCaptchaFieldXpath = `//*[contains(@class, 'swal2-input')]`;
const NotaryCaptchaSubmitXpath = `//*[contains(@class, 'swal2-confirm')]`;
const NotaryCancelCaptchaFillingXpath = `//*[contains(@class, 'swal2-cancel')][contains(text(),'ЗАКРЫТЬ')]`;
const NotaryResultNotFoundXpath = `//*[contains(@id, 'error-label')]`;
const NotaryLoadingSpinnerXpath = `//*[@id = 'spinner'][@class = 'spinner']`;
const NotaryCloseResultTableXpath = `//*[contains(text(), 'ЗАКРЫТЬ')]`;
const NotaryCheckChangesHistoryXpath = `//*[contains(text(), 'История изменений')]`;
const NotaryPDFLinkCountXpath = `//*[@class='sweet-text']//*/tr/td[1]//*[@href]`;
const NotaryGetPDFLinkXpath = (number) => `//*[@class='sweet-text']//*/tr[${number}]/td[1]//*[@href]`;
const NotarySecondResultTabXpath = `//*[contains(text(), 'Федеральный ресурс')]`;
const NotaryResultTablePledgorXpath = `//*[contains(@class, 'table-search')]//*/td[5]//*/a`;
const NotaryResultHeaderXpath = `//*[contains(@class, 'sweet-header')][contains(text(), 'Результаты поиска')]`;
const NotaryResultElementsCountXpath = `//*[contains(@class, 'participant-card')]//*/td[@data-v-6df97fcc]`;
const NotaryResultListNamesXpath = (number) => `//*[contains(@class, 'participant-card')]//*[contains(@class, 'table table-extra')]//*[contains(@index, '${number}')]//*/small[text()]`;
const NotaryResultListDataXpath = (number) => `//*[contains(@class, 'table table-extra')]//*[contains(@index, '${number}')]//*/span/span[last()][text()] | //*[contains(@class, 'table table-extra')]//*[contains(@index, '${number}')]//*/span/a[last()][text()]`;
const NotaryResultTableDataLengthXpath = (index) => `//*[contains(@class, 'participant-card')]//*[contains(@index, '${index}')]/td/span`;
const NotaryGetDataFromResultTableXpath = (index, number) => `//*[contains(@class, 'participant-card')]//*[contains(@index, '${index}')]/td/span[${number}]`;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

module.exports.CaptchaElementXpath = CaptchaElementXpath;
module.exports.VINFieldXpath = VINFieldXpath;

module.exports.CheckHistoryXpath = CheckHistoryXpath;
module.exports.WaitWhileCheckingHistoryXpath = WaitWhileCheckingHistoryXpath;
module.exports.CheckingHistoryResultXpath = CheckingHistoryResultXpath;
module.exports.CheckingHistoryResultNotFoundXpath = CheckingHistoryResultNotFoundXpath;
module.exports.CarDataListXpath = CarDataListXpath;
module.exports.CheckingHistoryErrorXpath = CheckingHistoryErrorXpath;
module.exports.OwnershipPeriodFromXpath = OwnershipPeriodFromXpath;
module.exports.OwnershipPeriodToXpath = OwnershipPeriodToXpath;
module.exports.CarLastActionXpath = CarLastActionXpath;
module.exports.DateOfInfoChecking = DateOfInfoChecking;

module.exports.CheckDTPHistoryBTNXpath = CheckDTPHistoryBTNXpath;
module.exports.WaitWhileCheckingDTPXpath = WaitWhileCheckingDTPXpath;
module.exports.CheckingDTPErrorXpath = CheckingDTPErrorXpath;
module.exports.CheckingDTPResultXpath = CheckingDTPResultXpath;
module.exports.CheckingDTPResultNotFoundXpath = CheckingDTPResultNotFoundXpath;
module.exports.GetDTPInfoNumberXpath = GetDTPInfoNumberXpath;
module.exports.GetDTPDataXpath = GetDTPDataXpath;
module.exports.ScreenDTPMapXpath = ScreenDTPMapXpath;
module.exports.DetailDTPInfoXpath = DetailDTPInfoXpath;
module.exports.MapImagePath = MapImagePath;
module.exports.DateOfDTPChecking = DateOfDTPChecking;
module.exports.DTPMapDescriptionXpath = DTPMapDescriptionXpath;

module.exports.CheckWantedStatusBTNXpath = CheckWantedStatusBTNXpath;
module.exports.WaitWhileCheckingWantedXpath = WaitWhileCheckingWantedXpath;
module.exports.CheckingWantedErrorXpath = CheckingWantedErrorXpath;
module.exports.CheckingWantedResultXpath = CheckingWantedResultXpath;
module.exports.CheckingWantedResultNotFoundXpath = CheckingWantedResultNotFoundXpath;
module.exports.DateOfWantedChecking = DateOfWantedChecking;

module.exports.CheckRestrictedStatusBTNXpath = CheckRestrictedStatusBTNXpath;
module.exports.WaitWhileCheckingRestrictionsXpath = WaitWhileCheckingRestrictionsXpath;
module.exports.CheckingRestrictionsErrorXpath = CheckingRestrictionsErrorXpath;
module.exports.CheckingRestrictionsResultXpath = CheckingRestrictionsResultXpath;
module.exports.CheckingRestrictionsResultNotFoundXpath = CheckingRestrictionsResultNotFoundXpath;
module.exports.DateOfRestrictedChecking = DateOfRestrictedChecking;

module.exports.AutoExtraDamageListXpath = AutoExtraDamageListXpath;
module.exports.AutoFullDamageListXpath = AutoFullDamageListXpath;
module.exports.RestrictedListXpath = RestrictedListXpath;
module.exports.RestrictedItemDataXpath = RestrictedItemDataXpath;
module.exports.RestrictedItemLinkXpath = RestrictedItemLinkXpath;

module.exports.GoogleReCaptchaNotCompleteXpath = GoogleReCaptchaNotCompleteXpath;

module.exports.RosStandardVINFieldXpath = RosStandardVINFieldXpath;
module.exports.RosStandardFindButtonXpath = RosStandardFindButtonXpath;
module.exports.RosInfoAboutVINNotFoundXpath = RosInfoAboutVINNotFoundXpath;
module.exports.RosOnlyLatinsXpath = RosOnlyLatinsXpath;

module.exports.CustomsVINFieldXpath = CustomsVINFieldXpath;
module.exports.CustomsFindButtonXpath = CustomsFindButtonXpath;
module.exports.CustomsTableResultXpath = CustomsTableResultXpath;

module.exports.MosAuthLoginFieldXpath = MosAuthLoginFieldXpath;
module.exports.MosAuthPasswordFieldXpath = MosAuthPasswordFieldXpath;
module.exports.MosAuthSubmitButtonXpath = MosAuthSubmitButtonXpath;
module.exports.MosVINorGosNumberFieldXpath = MosVINorGosNumberFieldXpath;
module.exports.MosSRTSFieldXpath = MosSRTSFieldXpath;
module.exports.MosSubmitButtonXpath = MosSubmitButtonXpath;
module.exports.MosCaptchaImageXpath = MosCaptchaImageXpath;
module.exports.MosCaptchaFieldXpath = MosCaptchaFieldXpath;
module.exports.MosCaptchaRefreshXpath = MosCaptchaRefreshXpath;
module.exports.MosVINorGosNumberErrorXpath = MosVINorGosNumberErrorXpath;
module.exports.MosPleaseEnterToSiteXpath = MosPleaseEnterToSiteXpath;
module.exports.MosResponseNotFoundXpath = MosResponseNotFoundXpath;
module.exports.MosCarPriceXpath = MosCarPriceXpath;
module.exports.MosCarMileageXpath = MosCarMileageXpath;
module.exports.MosCarMileageInfoXpath = MosCarMileageInfoXpath;
module.exports.MosCarTechnicalInspectionXpath = MosCarTechnicalInspectionXpath;

module.exports.NotarySearchInfoXpath = NotarySearchInfoXpath;
module.exports.NotaryINFieldXpath = NotaryINFieldXpath;
module.exports.NotarySubmitButtonXpath = NotarySubmitButtonXpath;
module.exports.NotaryCaptchaImageXpath = NotaryCaptchaImageXpath;
module.exports.NotaryCaptchaFieldXpath = NotaryCaptchaFieldXpath;
module.exports.NotaryCaptchaSubmitXpath = NotaryCaptchaSubmitXpath;
module.exports.NotaryCancelCaptchaFillingXpath = NotaryCancelCaptchaFillingXpath;
module.exports.NotaryResultNotFoundXpath = NotaryResultNotFoundXpath;
module.exports.NotaryLoadingSpinnerXpath = NotaryLoadingSpinnerXpath;
module.exports.NotaryCloseResultTableXpath = NotaryCloseResultTableXpath;
module.exports.NotaryCheckChangesHistoryXpath = NotaryCheckChangesHistoryXpath;
module.exports.NotaryPDFLinkCountXpath = NotaryPDFLinkCountXpath;
module.exports.NotaryGetPDFLinkXpath = NotaryGetPDFLinkXpath;
module.exports.NotarySecondResultTabXpath = NotarySecondResultTabXpath;
module.exports.NotaryResultTablePledgorXpath = NotaryResultTablePledgorXpath;
module.exports.NotaryResultHeaderXpath = NotaryResultHeaderXpath;
module.exports.NotaryResultElementsCountXpath = NotaryResultElementsCountXpath;
module.exports.NotaryResultListNamesXpath = NotaryResultListNamesXpath;
module.exports.NotaryResultListDataXpath = NotaryResultListDataXpath;
module.exports.NotaryResultTableDataLengthXpath = NotaryResultTableDataLengthXpath;
module.exports.NotaryGetDataFromResultTableXpath = NotaryGetDataFromResultTableXpath;
