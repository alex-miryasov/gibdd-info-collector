const axios = require('axios');
const converter = require('base64topdf');
const webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const proxy = require('selenium-webdriver/proxy');
const chromium = require('chromium');
require('chromedriver');

const fs = require('fs');
const Downloader = require('./Downloader');
const downloader = new Downloader();

const { By, until, Key } = webdriver;

const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();

const { default: PQueue } = require('p-queue');
const GetCarHistoryQueue = new PQueue({ concurrency: 10 });
const GetCarDTPInfoQueue = new PQueue({ concurrency: 1 });
const GetCarWantedStatusQueue = new PQueue({ concurrency: 1 });
const GetCarRestrictedStatusQueue = new PQueue({ concurrency: 1 });
const GetAllDataAboutCarQueue = new PQueue({ concurrency: 1 });
const GetInfoFromGosStandardQueue = new PQueue({ concurrency: 1 });
const GetInfoFromCustomsQueue = new PQueue({ concurrency: 1 });
const CheckStatusQueue = new PQueue({ concurrency: 2 });

const database = require('quick.db');
const taskDB = new database.table('Tasks');
const vinTaskDB = new database.table('VINTasks');
const tsTypeDB = new database.table('TSTypes');

const pdfParsing = require('pdf-parse');

const port = 4893;
const mainURL = 'https://xn--90adear.xn--p1ai/check/auto';
const gostURL = 'http://easy.gost.ru/';
const customsURL = 'https://customsonline.ru/vvoz_avto.html#';
const mosURL = 'https://www.mos.ru/services/autohistory/check';
const notaryURL = `https://www.reestr-zalogov.ru/search/index`;

const appName = 'GIBDD Data Collector';

const solver = require('2captcha');
const { DTPMapDescriptionXpath } = require('./Paths');
const { NotaryGetPDFLinkXpath } = require('./Paths');
const { NotaryCloseResultTableXpath } = require('./Paths');
const { NotaryCheckChangesHistoryXpath } = require('./Paths');
const { NotaryPDFLinkCountXpath } = require('./Paths');
const { LoadFileByURL } = require('./helpers');
const { NotaryGetDataFromResultTableXpath } = require('./Paths');
const { NotaryResultTableDataLengthXpath } = require('./Paths');
const { NotaryResultListDataXpath } = require('./Paths');
const { NotaryResultListNamesXpath } = require('./Paths');
const { NotaryResultElementsCountXpath } = require('./Paths');
const { GetElementsLength } = require('./helpers');
const { NotaryResultHeaderXpath } = require('./Paths');
const { NotaryResultTablePledgorXpath } = require('./Paths');
const { NotarySecondResultTabXpath } = require('./Paths');
const { NotaryCancelCaptchaFillingXpath } = require('./Paths');
const { DecodeCaptcha } = require('./Methods');
const { vins } = require('./VINs');
const { ProxyGroup, SpecialProxy } = require('./Proxies');

const { GetErrorVIN, GetProxyQueue, GetProxyData, GetTextFromCaptcha, CalculateDate } = require('./Methods');
const {
  SendValueToField, ClickToObject, WaitWhileNotDisplayed, WaitWhileDisplayed, DeleteElementFromDOM, GetDataToArray, TakeElementScreensShot, ChangeTaskItemValue,
  ScrollToElement, Sleep, CreateCollectionID, FirstDriverAction, SendToAttributeValue, RandomNumber, ChangeTaskItemValue2
} = require('./helpers');

const {
  CaptchaElementXpath, VINFieldXpath, CheckingHistoryResultNotFoundXpath, CheckingHistoryErrorXpath, CheckingHistoryResultXpath, WaitWhileCheckingHistoryXpath, CheckHistoryXpath,
  RestrictedItemLinkXpath, CarDataListXpath, OwnershipPeriodFromXpath, OwnershipPeriodToXpath, CarLastActionXpath, DateOfInfoChecking, CheckDTPHistoryBTNXpath, WaitWhileCheckingDTPXpath,
  DateOfRestrictedChecking, RestrictedItemDataXpath, RestrictedListXpath, CheckRestrictedStatusBTNXpath, WaitWhileCheckingRestrictionsXpath, CheckingRestrictionsResultXpath, CheckingRestrictionsResultNotFoundXpath,
  CheckingRestrictionsErrorXpath, CheckingWantedResultNotFoundXpath, CheckingWantedResultXpath, CheckingWantedErrorXpath, WaitWhileCheckingWantedXpath, CheckWantedStatusBTNXpath, DetailDTPInfoXpath,
  DateOfDTPChecking, AutoFullDamageListXpath, AutoExtraDamageListXpath, GetDTPDataXpath, MapImagePath, GetDTPInfoNumberXpath, CheckingDTPResultNotFoundXpath, CheckingDTPResultXpath, CheckingDTPErrorXpath,
  RosStandardVINFieldXpath, RosStandardFindButtonXpath, RosInfoAboutVINNotFoundXpath, CustomsVINFieldXpath, CustomsFindButtonXpath, CustomsTableResultXpath, GoogleReCaptchaNotCompleteXpath,
  MosVINorGosNumberFieldXpath, MosSRTSFieldXpath, MosSubmitButtonXpath, MosAuthLoginFieldXpath, MosAuthPasswordFieldXpath, MosAuthSubmitButtonXpath, MosCaptchaImageXpath, MosCaptchaFieldXpath, MosCaptchaRefreshXpath,
  MosCarPriceXpath, MosCarMileageXpath, MosCarMileageInfoXpath, MosCarTechnicalInspectionXpath, MosVINorGosNumberErrorXpath, MosPleaseEnterToSiteXpath, MosResponseNotFoundXpath, RosOnlyLatinsXpath,
  NotarySearchInfoXpath, NotaryINFieldXpath, NotarySubmitButtonXpath, NotaryCaptchaImageXpath, NotaryCaptchaFieldXpath, NotaryCaptchaSubmitXpath, NotaryResultNotFoundXpath, NotaryLoadingSpinnerXpath
} = require('./Paths');

async function GetInfoAboutAutoHistory (driver, id, vin, database) {
  await console.log('GETTING INFO ABOUT OWNERSHIP HISTORY');

  await ChangeTaskItemValue2(taskDB, id, vin, 'checking');

  await WaitWhileDisplayed(driver, `//*[contains(text(), 'Идет загрузка...')]`, true, 'ИДЕТ ЗАГРУЗКА...', 'ЗАГРУЗКА ЗАВЕРШЕНА', true);

  try {
    let notResponses = false;
    await driver.wait(webdriver.until.elementLocated(By.xpath(`//*[contains(text(), 'Идет загрузка...')]`)), 2000).then(async () => {
      if (!!await driver.findElement(By.xpath(`//*[contains(text(), 'Идет загрузка...')]`)).isDisplayed()) {
        await ChangeTaskItemValue(database, id, vin, 'waiting', undefined, 'GIBDD.RF NOT RESPONSE', undefined, undefined);
        notResponses = true;
      }
    });
    if (!!notResponses)
      return;
  } catch (e) {
    await console.log('ITS RESPONSES');
  }

  await FirstDriverAction(driver, database, id, vin, VINFieldXpath, CheckHistoryXpath, WaitWhileCheckingHistoryXpath, CheckingHistoryErrorXpath, CheckingHistoryResultXpath, CheckingHistoryResultNotFoundXpath);

  if (await database.get(`task${id}.status`) === 'error') {
    await console.log('STATUS IS ERROR');
    return;
  }

  let carDataArr = await GetDataToArray(driver, CarDataListXpath);
  await console.log('ALL DATA: ', carDataArr);

  let carData = {
    markModel: carDataArr[0],
    releaseYear: carDataArr[1],
    vin: carDataArr[2],
    bodyNum: carDataArr[3],
    chassis: carDataArr[4],
    power: carDataArr[7],
    category: carDataArr[8]
  };
  await ChangeTaskItemValue(database, id, vin, 'receiving', undefined, carData);

  let dbType = await tsTypeDB.get(`type`);
  for (let ii = 0; ii < dbType.length; ii++) {
    if (await carData.category !== await dbType[ii]) {
      let newTypeInDB = await dbType.push(await carData.category);
      await tsTypeDB.set(`type`, await newTypeInDB);
      await console.log('ADDED NEW TYPE: ', await carData.category);
      await console.log('ALL TYPES: ', await tsTypeDB.get(`type`));
    }
  }

  let PeriodFromArr = await GetDataToArray(driver, OwnershipPeriodFromXpath);
  let PeriodToArr = await GetDataToArray(driver, OwnershipPeriodToXpath);

  let fromtoArr = [];
  for (let i = 0; i < PeriodToArr.length; i++) {
    fromtoArr[i] = {
      dates: 'С: ' + await PeriodFromArr[i] + ' ПО: ' + await PeriodToArr[i],
      pause: (PeriodToArr.length > 1 && PeriodToArr[i] !== 'настоящее' && !!PeriodFromArr[i + 1]) ? await CalculateDate(await PeriodToArr[i], await PeriodFromArr[i + 1]) : undefined
    };

    await console.log('FROM: ' + await PeriodFromArr[i] + ' TO: ' + await PeriodToArr[i]);
  }

  let lastAct = await driver.findElement(By.xpath(CarLastActionXpath)).getText();

  let ownershipList = {
    fromto: fromtoArr,
    lastAction: lastAct
  };

  let checkDate = await driver.findElement(By.xpath(DateOfInfoChecking)).getText();
  await console.log('CHECKING DATE: ', await checkDate);

  await ChangeTaskItemValue(database, id, vin, 'waiting', undefined, carData, ownershipList, await checkDate);
}

async function GetInfoAboutAutoDTP (driver, id, vin) {
  await console.log('GETTING INFO ABOUT DTP');

  await ChangeTaskItemValue2(taskDB, id, vin, 'checking');

  await FirstDriverAction(driver, taskDB, id, vin, VINFieldXpath, CheckDTPHistoryBTNXpath, WaitWhileCheckingDTPXpath, CheckingDTPErrorXpath, CheckingDTPResultXpath, CheckingDTPResultNotFoundXpath);

  if (await taskDB.get(`task${id}.status`) === 'error') {
    await console.log('STATUS IS ERROR');
    await RecheckingErrorVIN(id, vin, taskDB, 'dtp');
    return;
  }

  try {
    await driver.wait(until.elementLocated(By.xpath(`//*[contains(text(),'записи о дорожно-транспортных происшествиях не найдены')]`)), 2500).then(async () => {
      let simpleResultDisplayed = await driver.findElement(By.xpath(`//*[contains(text(),'записи о дорожно-транспортных происшествиях не найдены')]`)).isDisplayed();
      let simpleResultText = await driver.findElement(By.xpath(`//*[contains(text(),'записи о дорожно-транспортных происшествиях не найдены')]`)).getText();
      if (!!await simpleResultDisplayed) {
        await console.log('SIMPLE RESULT IS VISIBLE');
        await ChangeTaskItemValue2(taskDB, id, vin, 'complete', 'dtp', await simpleResultText);
      }
    });
    return;
  } catch (err) {
    await console.log('SIMPLE RESULT NOT FOUND');
  }

  let dtpNum = await GetDataToArray(driver, GetDTPInfoNumberXpath);

  let dtpMaps = [];
  for (let k = 0; k < dtpNum.length; k++) {
    try {
      await driver.wait(until.elementLocated(By.xpath(MapImagePath(dtpNum[k]))), 3000).then(async () => {
        let element = await driver.findElement(By.xpath(MapImagePath(dtpNum[k])));
        await ScrollToElement(driver, MapImagePath(dtpNum[k]));
        await element.takeScreenshot().then(async function (image, err) {
          if (!err) {
            dtpMaps[k] = await image.toString('base64');
            await console.log('DTP SCREEN SAVED');
          } else await console.log('ERROR: ', err);
        });
      });
    } catch (e) {
      await console.log('MAP NOT FOUND');
    }
  }
  await console.log('DTP NUM: ', dtpNum);

  let dtpDataArr = [];
  for (let i = 0; i < dtpNum.length; i++) {
    dtpDataArr[i] = await GetDataToArray(driver, await GetDTPDataXpath(dtpNum[i]));
  }

  let dtpDataTxt;

  try {
    await driver.wait(until.elementLocated(By.xpath(DTPMapDescriptionXpath)), 3000).then(async () => {
      let element = await driver.findElement(By.xpath(DTPMapDescriptionXpath));
      await ScrollToElement(driver, DTPMapDescriptionXpath);
      await element.takeScreenshot().then(async function (image, err) {
        if (!err) {
          dtpDataTxt = await image.toString('base64');
          await console.log('DTP TEXT SCREEN SAVED');
        } else await console.log('ERROR: ', err);
      });
    });
  } catch (e) {
    await console.log('MAP NOT FOUND');
  }

  let carFullDamageInfo;
  let autoExtraDamageList;

  let dtpData = [];
  for (let i = 0; i < dtpDataArr.length; i++) {
    dtpData[i] = {
      dtpDate: await dtpDataArr[i][0],
      dtpType: await dtpDataArr[i][1],
      dtpRegion: await dtpDataArr[i][2],
      dtpCarMark: await dtpDataArr[i][3],
      dtpCarReleaseYear: await dtpDataArr[i][4],
      dtpMap: await dtpMaps[i],
      dtpMapDescription: await dtpDataTxt
    };
  }

  let fullDTPInfo = {
    info: dtpData,
    extraDamageInfo: autoExtraDamageList,
    fullDamageInfo: carFullDamageInfo
  };

  let checkDate = await driver.findElement(By.xpath(DateOfDTPChecking)).getText();
  await console.log('CHECKING DATE: ', await checkDate);

  await ChangeTaskItemValue(taskDB, id, vin, 'complete', undefined, undefined, undefined, await checkDate, fullDTPInfo);
}

async function GetWantedStatus (driver, id, vin) {
  await console.log('GETTING CAR WANTED STATUS');

  await ChangeTaskItemValue2(taskDB, id, vin, 'checking');

  await FirstDriverAction(driver, taskDB, id, vin, VINFieldXpath, CheckWantedStatusBTNXpath, WaitWhileCheckingWantedXpath, CheckingWantedErrorXpath, CheckingWantedResultXpath, CheckingWantedResultNotFoundXpath);

  if (await taskDB.get(`task${id}.status`) === 'error') {
    await console.log('STATUS IS ERROR');
    return;
  }

  try {
    await driver.wait(until.elementLocated(By.xpath(`//*[contains(text(),'не найдена информация о розыске транспортного средства')]`)), 2500).then(async () => {
      let simpleResultDisplayed = await driver.findElement(By.xpath(`//*[contains(text(),'не найдена информация о розыске транспортного средства')]`)).isDisplayed();
      let simpleResultText = await driver.findElement(By.xpath(`//*[contains(text(),'не найдена информация о розыске транспортного средства')]`)).getText();
      if (!!await simpleResultDisplayed) {
        await console.log('SIMPLE RESULT IS VISIBLE');
        await ChangeTaskItemValue2(taskDB, id, vin, 'complete', 'dtp', await simpleResultText);
      }
    });
    return;
  } catch (err) {
    await console.log('SIMPLE RESULT NOT FOUND');
    await ChangeTaskItemValue2(taskDB, id, vin, 'error', 'error', 'MAY BE WANTED STATUS IS FOUND');
  }
}

async function GetRestrictedStatus (driver, id, vin) {
  await console.log('GETTING RESTRICTIONS ON CAR');

  await ChangeTaskItemValue2(taskDB, id, vin, 'checking');

  await FirstDriverAction(driver, taskDB, id, vin, VINFieldXpath, CheckRestrictedStatusBTNXpath, WaitWhileCheckingRestrictionsXpath, CheckingRestrictionsErrorXpath, CheckingRestrictionsResultXpath, CheckingRestrictionsResultNotFoundXpath);

  if (await taskDB.get(`task${id}.status`) === 'error') {
    await console.log('STATUS IS ERROR');
    await RecheckingErrorVIN(id, vin, taskDB, 'restrictions');
    return;
  }

  try {
    await driver.wait(until.elementLocated(By.xpath(`//*[@id='checkAutoRestricted']//*[contains(text(),'По указанному VIN (номер кузова или шасси) не найдена')]`)), 2500).then(async () => {
      let simpleResultDisplayed = await driver.findElement(By.xpath(`//*[@id='checkAutoRestricted']//*[contains(text(),'По указанному VIN (номер кузова или шасси) не найдена')]`)).isDisplayed();
      let simpleResultText = await driver.findElement(By.xpath(`//*[@id='checkAutoRestricted']//*[contains(text(),'По указанному VIN (номер кузова или шасси) не найдена')]`)).getText();
      if (!!await simpleResultDisplayed) {
        await console.log('SIMPLE RESULT IS VISIBLE');
        await ChangeTaskItemValue2(taskDB, id, vin, 'complete', 'restrictions', await simpleResultText);
      }
    });
    return;
  } catch (err) {
    await console.log('RESTRICTION BASE RESULT NOT FOUND');
    await ChangeTaskItemValue2(taskDB, id, vin, 'error', 'error', 'MAY BE WANTED STATUS IS FOUND');
  }

  let restrictedListArr = [];
  try {
    restrictedListArr = await GetDataToArray(driver, RestrictedListXpath);
  } catch (e) {
    await console.log('COLLECTING RESTRICTION DATA ERROR');
  }
  let restrictedItemDataArr = [];
  try {
    for (let i = 0; i < restrictedListArr.length; i++) {
      restrictedItemDataArr[i] = await GetDataToArray(driver, await RestrictedItemDataXpath(i + 1));
    }
    await console.log('RESTRICTED ITEM DATA: ', await restrictedItemDataArr);
  } catch (e) {
    await console.log('COLLECTING RESTRICTION ITEM DATA ERROR');
  }
  let RestrictionsData = [];
  for (let k = 0; k < restrictedItemDataArr.length; k++) {
    RestrictionsData[k] = {
      markModel: await restrictedItemDataArr[k][0],
      releaseYear: await restrictedItemDataArr[k][1],
      restrictionDate: await restrictedItemDataArr[k][2],
      restrictionRegion: await restrictedItemDataArr[k][3],
      restrictionBy: await restrictedItemDataArr[k][4],
      restrictionType: await restrictedItemDataArr[k][5],
      whyRestricted: await restrictedItemDataArr[k][6],
      initiatorPhone: await restrictedItemDataArr[k][7],
      gibddKey: await restrictedItemDataArr[k][8],
      fsspData: await GetDataFromFSSP(driver, k + 1)
    };
  }

  await ScrollToElement(driver, DateOfRestrictedChecking);
  let checkDate = await driver.findElement(By.xpath(DateOfRestrictedChecking)).getText();
  await console.log('CHECKING DATE: ', await checkDate);

  await ChangeTaskItemValue2(taskDB, id, vin, 'collected', 'checkDate', await checkDate);
  await ChangeTaskItemValue2(taskDB, id, vin, 'collected', 'restrictions', RestrictionsData);
}

async function GetDataFromFSSP (driver, num) {
  let fsspBankNumber = await driver.findElement(By.xpath(await RestrictedItemLinkXpath(num))).getText();
  await console.log('BANK NUMBER: ', await fsspBankNumber);
  return await CreateFSSPTask(await fsspBankNumber);
}

async function CreateFSSPTask (number) {
  let taskNumber;
  await axios({
    method: 'get',
    url: 'https://api-ip.fssprus.ru/api/v1.0/search/ip',
    params: {
      token: 'n1iqF1ofERJE',
      number: await number,
    }
  }).then(async function (resp) {
    const { data: { response: { task } } } = await resp;
    await console.log('FSSP RESPONSE TASK NUMBER: ', await task);
    taskNumber = await task;
  }).catch(async function () {
    await console.log('RESPONSE NOT RETURNED');
  });

  let dataArr = [];
  await dataArr.push(await CheckFSSPTaskStatus(await taskNumber));
  return dataArr;
}

async function CheckFSSPTaskStatus (task) {
  await Sleep(2000);

  let taskData;
  await axios({
    method: 'get',
    url: 'https://api-ip.fssprus.ru/api/v1.0/status',
    params: {
      token: 'n1iqF1ofERJE',
      task: task
    }
  }).then(async function (response) {
    const { data: { status } } = await response;
    await console.log('FSSP RESPONSE TASK STATUS: ', await status);

    if (await status === 'success') {
      await axios({
        method: 'get',
        url: 'https://api-ip.fssprus.ru/api/v1.0/result',
        params: {
          token: 'n1iqF1ofERJE',
          task: await task
        }
      }).then(async function (response) {
        const { data: { response: { result } } } = await response;
        await console.log('FSSP RESPONSE TASK RESULT LENGTH: ', await result[0].result.length);
        await console.log('FSSP RESPONSE TASK RESULT: ', await result[0].result[0]);
        taskData = await result[0].result[0];
      }).catch(async function () {
        await console.log('RESPONSE NOT RETURNED');
      });
    }
  }).catch(async function () {
    await console.log('RESPONSE NOT RETURNED');
  });

  return await taskData;
}

async function CheckGosStandardInfoByVIN (driver, id, vin, taskDB) {
  await console.log('CHECKING INFO IN GOS STANDARD BY VIN');
  await ChangeTaskItemValue(taskDB, id, vin, 'processing');
  await driver.sleep(2000);
  await WaitWhileNotDisplayed(driver, RosStandardVINFieldXpath, false);
  await SendValueToField(driver, RosStandardVINFieldXpath, vin, false);

  try {
    await driver.wait(until.elementLocated(By.xpath(RosOnlyLatinsXpath)), 2000).then(async () => {
      if (!!await driver.findElement(By.xpath(RosOnlyLatinsXpath)).isDisplayed()) {
        let errorText = await driver.findElement(By.xpath(RosOnlyLatinsXpath)).getText();
        await ChangeTaskItemValue2(taskDB, id, vin, 'complete', 'mos', await errorText);
      } else {
        await console.log('ERROR INVISIBLE');
      }
    });
  } catch (e) {
    await console.log('VIN NOT FOUND IN GOS STANDARD');
  }

  await ChangeTaskItemValue(taskDB, id, vin, 'waiting');
  try {
    await driver.wait(webdriver.until.elementLocated(By.xpath(RosInfoAboutVINNotFoundXpath)), 5000).then(async element => {
      await driver.wait(webdriver.until.elementIsEnabled(element), 2000).then(async () => {
        let responseText = await driver.findElement(By.xpath(RosInfoAboutVINNotFoundXpath)).getText();
        await console.log('RESPONSE TEXT: ', await responseText);
        await ChangeTaskItemValue(taskDB, id, vin, 'complete', undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, await responseText);
      });
    });
  } catch (err) {
    await console.log('OTHER RESPONSE: ', await err);
    await ChangeTaskItemValue(taskDB, id, vin, 'error', 'OTHER RESPONSE');
  }
}

async function CheckCustomsInfoVyVIN (driver, id, vin, taskDB) {
  await console.log('CHECKING INFO IN CUSTOMS BY VIN');
  await ChangeTaskItemValue(taskDB, id, vin, 'processing');
  await driver.sleep(2000);
  await WaitWhileNotDisplayed(driver, CustomsVINFieldXpath);
  await SendValueToField(driver, CustomsVINFieldXpath, vin);
  await ClickToObject(driver, CustomsFindButtonXpath);
  await ChangeTaskItemValue(taskDB, id, vin, 'waiting');
  try {
    await driver.wait(webdriver.until.elementLocated(By.xpath(CustomsTableResultXpath)), 5000).then(async element => {
      await driver.wait(webdriver.until.elementIsEnabled(element), 2000).then(async () => {
        // await ScrollToElement(driver, CustomsTableResultXpath);
        await driver.sleep(1500);
        let responseText = await GetDataToArray(driver, CustomsTableResultXpath, true);
        await console.log('RESPONSE TEXT: ', responseText);
        if (await responseText[0] === '<<\n<\n>\n>>') {
          await ChangeTaskItemValue(taskDB, id, vin, 'complete', undefined, undefined, undefined, undefined, undefined, undefined, undefined, 'VIN NOT FOUND IN CUSTOMS TABLE');
          return;
        }
        let customsData = {
          entryDate: (await responseText[1] !== '-') ? await responseText[1] : undefined,
          vinNumber: (await responseText[2] !== '-') ? await responseText[2] : undefined,
          bodyNumber: (await responseText[3] !== '-') ? await responseText[3] : undefined,
          chassisNumber: (await responseText[4] !== '-') ? await responseText[4] : undefined,
          fromCountry: (await responseText[5] !== '-') ? await responseText[5] : undefined,
        };

        await ChangeTaskItemValue(taskDB, id, vin, 'complete', undefined, undefined, undefined, undefined, undefined, undefined, undefined, await customsData);
      });
    });
  } catch (err) {
    await console.log('VIN NOT FOUND IN CUSTOMS DATABASE');
    await ChangeTaskItemValue(taskDB, id, vin, 'complete', undefined, undefined, undefined, undefined, undefined, undefined, undefined, 'VIN NOT FOUND IN CUSTOMS TABLE');
  }
}

async function ParseVINNumbersFromFile (array, count, odlVIN, oldID, counter) {
  let idsBox = [];

  let dbIDs = await vinTaskDB.get(`idsBox`);

  idsBox = (!!await dbIDs) ? await dbIDs : idsBox;

  for (let ii = 0; ii < 150; ii++) {
    let vin = (!odlVIN) ? await GetVIN(await count) : odlVIN;
    let id = (!oldID) ? await CreateCollectionID() : oldID;

    await console.log('PARSING ID: ', await id);
    await console.log('PARSING VIN: ', await vin);
    await idsBox.push(await id);
    await vinTaskDB.set(`idsBox`, idsBox);
    await ChangeTaskItemValue(vinTaskDB, await id, await vin, 'processing');

    await StartParsingData(id, vin);

    await console.log('REQUESTS PAUSE');
    await Sleep(45000);
  }
}

async function GetVIN (count) {
  let idsBox = await vinTaskDB.get(`idsBox`);
  let DBData = [];

  for (let i = 0; i < idsBox.length; i++) {
    await DBData.push(await vinTaskDB.get(`task${idsBox[i]}`));
  }

  let returnVIN = await vins[DBData.length + await count];
  await console.log('RETURN VIN: ', returnVIN);
  return await returnVIN;
}

async function StartParsingData (id, vin) {
  let proxyData = await GetProxyData();
  let ProxyQueue = await GetProxyQueue();
  await ProxyQueue.add(async function () {
    let driver = await CreateDriver(mainURL, 'chrome', proxyData.address, proxyData.login, proxyData.password);
    // await GetInfoAboutAutoHistory(driver, id, vin, vinTaskDB).then(async () => {
    //   let checkStatus = await vinTaskDB.get(`task${id}.status`);
    //   if (checkStatus !== 'error') {
    //     await DestroyDriver(driver);
    //     await console.log('VIN TASK SAVED');
    //   } else {
    //     await DestroyDriver(driver);
    //     await console.log('RESTARTING');
    //     await RecheckingErrorVIN(id, vin, vinTaskDB);
    //   }
    // });
    await GetInfoAboutAutoHistory(driver, id, vin, vinTaskDB).then(async () => {
      await CheckOnOtherServices(driver, id, vin, vinTaskDB).then(async () => {
        driver = await CreateDriver(mosURL, 'chrome', proxyData.address, proxyData.login, proxyData.password, true);
        await CheckVINOnMos(driver, id, vin, vinTaskDB).then(async () => {
          await DestroyDriver(driver);
        });
      });
    });
  });
}

async function RecheckingErrorVIN (id, vin, database, checkType) {
  await console.log('STARTING RECHECKING');
  for (let ii = 0; ii < SpecialProxy.length; ii++) {
    let driver = await CreateDriver(mainURL, 'chrome', await SpecialProxy[ii].address, await SpecialProxy[ii].login, await SpecialProxy[ii].password);
    if (checkType === 'history') {
      await GetInfoAboutAutoHistory(driver, id, vin, database).then(async () => {
        await console.log('AFTER RECHECKING, STATUS: ', await database.get(`task${id}.status`));
      });
    } else if (checkType === 'restrictions') {
      await GetRestrictedStatus(driver, id, vin).then(async () => {
        await console.log('AFTER RECHECKING, STATUS: ', await database.get(`task${id}.status`));
      });
    } else if (checkType === 'dtp') {
      await GetInfoAboutAutoDTP(driver, id, vin).then(async () => {
        await console.log('AFTER RECHECKING, STATUS: ', await database.get(`task${id}.status`));
      });
    }
    let checkStatus = await database.get(`task${id}.status`);
    if (await checkStatus === 'collected' || await checkStatus === 'waiting') {
      await DestroyDriver(driver);
      return console.log(`VIN ${vin} RECHECKED`);
    }
    await DestroyDriver(driver);
  }
  let driver = await CreateDriver(mainURL, 'chrome', undefined, undefined, undefined, true);
  await GetInfoAboutAutoHistory(driver, id, vin, vinTaskDB).then(async () => {
    await console.log('BROWSER WITH COOKIE COMPLETE OPERATION');
  });
}

async function CheckVINOnMos (driver, id, vin, taskDB) {
  await ChangeTaskItemValue(taskDB, id, vin, 'processing');
  await driver.sleep(1500);

  await driver.sleep(2000);

  try {
    await driver.wait(until.elementLocated(By.xpath(MosPleaseEnterToSiteXpath)), 2000).then(async () => {
      if (!!await driver.findElement(By.xpath(MosPleaseEnterToSiteXpath)).isDisplayed()) {
        await driver.findElement(By.xpath(MosPleaseEnterToSiteXpath)).click();
      }
    });
  } catch (e) {
    await console.log('PLEASE ENTER NOT DISPLAYED');
  }

  try {
    await driver.wait(until.elementLocated(By.xpath(`//*[@id='password'][@name='password'][@type='password']`)), 5000).then(async () => {
      if (!!await driver.findElement(By.xpath(`//*[@id='password'][@name='password'][@type='password']`)).isDisplayed()) {
        await driver.findElement(By.xpath(`//*[@id='password'][@name='password'][@type='password']`)).sendKeys('IgorParol123');
        await driver.findElement(By.xpath(`//*[@id='bind'][@type='button'][contains(text(),'Войти')]`)).click();
      }
    });
  } catch (e) {
    await console.log('PLEASE ENTER NOT DISPLAYED');
  }

  await WaitWhileNotDisplayed(driver, MosVINorGosNumberFieldXpath, false);

  await SendValueToField(driver, MosVINorGosNumberFieldXpath, vin, false);
  await SendValueToField(driver, MosSRTSFieldXpath, '5423862899', false);

  try {
    if (!!await driver.findElement(By.xpath(MosCaptchaImageXpath)).isDisplayed()) {
      let captchaText = await GetTextFromCaptcha(driver, MosCaptchaImageXpath, MosCaptchaRefreshXpath);
      await SendValueToField(driver, MosCaptchaFieldXpath, await captchaText, false);
    }
  } catch (e) {
    await console.log('CAPTCHA NOT DISPLAYED');
  }
  await ClickToObject(driver, MosSubmitButtonXpath, false);

  await WaitWhileDisplayed(driver, `//*[@class='mos-loader']`);

  try {
    await driver.wait(until.elementLocated(By.xpath(`//*[@class='autohistory-search__error-message']`)), 5000).then(async () => {
      if (!!await driver.findElement(By.xpath(`//*[@class='autohistory-search__error-message']`)).isDisplayed()) {
        let errorText = await driver.findElement(By.xpath(`//*[@class='autohistory-search__error-message']`)).getText();
        await console.log('ERROR: ', await errorText);
        await ChangeTaskItemValue2(taskDB, id, vin, 'complete', 'mos', await errorText);
      }
    });
    return;
  } catch (e) {
    await console.log('PLEASE ENTER NOT DISPLAYED');
  }

  try {
    await driver.wait(until.elementLocated(By.xpath(MosVINorGosNumberErrorXpath)), 2000).then(async () => {
      if (!!await driver.findElement(By.xpath(MosVINorGosNumberErrorXpath)).isDisplayed()) {
        let errorText = await driver.findElement(By.xpath(MosVINorGosNumberErrorXpath)).getText();
        await ChangeTaskItemValue2(taskDB, id, vin, 'complete', 'mos', await errorText);
      }
    });
    return;
  } catch (e) {
    await console.log('VIN or GOS NUMBER IS VALID');
  }

  try {
    await driver.wait(until.elementLocated(By.xpath(MosResponseNotFoundXpath)), 2000).then(async () => {
      if (!!await driver.findElement(By.xpath(MosResponseNotFoundXpath)).isDisplayed()) {
        let errorText = await driver.findElement(By.xpath(MosResponseNotFoundXpath)).getText();
        await ChangeTaskItemValue2(taskDB, id, vin, 'complete', 'mos', await errorText);
      }
    });
    return;
  } catch (e) {
    await console.log('DATA FROM VIN or GOS NUMBER NOT FOUND');
  }

  await WaitWhileNotDisplayed(driver, MosCarMileageXpath, false);

  let CarPrice;
  try {
    await driver.wait(until.elementLocated(By.xpath(MosResponseNotFoundXpath)), 1500).then(async () => {
      if (!!await driver.findElement(By.xpath(MosResponseNotFoundXpath)).isDisplayed()) {
        CarPrice = await driver.findElement(By.xpath(MosCarPriceXpath)).getText();
        await console.log('CAR PRICE: ', await CarPrice);
      }
    });
  } catch (e) {
    await console.log('CAR PRICE NOT VISIBLE');
  }

  let CarMileage = await driver.findElement(By.xpath(MosCarMileageXpath)).getText();
  await console.log('CAR MILEAGE: ', await CarMileage);

  let CarMileageInfoData = await GetDataToArray(driver, MosCarMileageInfoXpath, true);
  await console.log('CAR MILEAGE DATA: ', await CarMileageInfoData);

  let CarMTechnicalInspectionData = await GetDataToArray(driver, MosCarTechnicalInspectionXpath, true);
  await console.log('CAR TECHNICAL INSPECTION: ', await CarMTechnicalInspectionData);

  let CarMileageInfo = [
    {
      date: (!!CarMileageInfoData[0]) ? CarMileageInfoData[0] : undefined,
      mileage: (!!CarMileageInfoData[1]) ? CarMileageInfoData[1] : undefined,
      commentary: (!!CarMileageInfoData[2] && CarMileageInfoData[2] !== '-') ? CarMileageInfoData[2] : undefined
    },
    {
      date: (!!CarMileageInfoData[3]) ? CarMileageInfoData[3] : undefined,
      mileage: (!!CarMileageInfoData[4]) ? CarMileageInfoData[4] : undefined,
      commentary: (!!CarMileageInfoData[5] && CarMileageInfoData[5] !== '-') ? CarMileageInfoData[5] : undefined
    },
    {
      date: (!!CarMileageInfoData[6]) ? CarMileageInfoData[6] : undefined,
      mileage: (!!CarMileageInfoData[7]) ? CarMileageInfoData[7] : undefined,
      commentary: (!!CarMileageInfoData[8] && CarMileageInfoData[8] !== '-') ? CarMileageInfoData[8] : undefined
    }
  ];
  let CarTechnicalInspection = [
    {
      date: (!!CarMTechnicalInspectionData[0]) ? CarMTechnicalInspectionData[0] : undefined,
      place: (!!CarMTechnicalInspectionData[1]) ? CarMTechnicalInspectionData[1] : undefined,
      diagnosticCard: (!!CarMTechnicalInspectionData[2]) ? CarMTechnicalInspectionData[2] : undefined
    },
    {
      date: (!!CarMTechnicalInspectionData[3]) ? CarMTechnicalInspectionData[3] : undefined,
      place: (!!CarMTechnicalInspectionData[4]) ? CarMTechnicalInspectionData[4] : undefined,
      diagnosticCard: (!!CarMTechnicalInspectionData[5]) ? CarMTechnicalInspectionData[5] : undefined
    },
    {
      date: (!!CarMTechnicalInspectionData[6]) ? CarMTechnicalInspectionData[6] : undefined,
      place: (!!CarMTechnicalInspectionData[7]) ? CarMTechnicalInspectionData[7] : undefined,
      diagnosticCard: (!!CarMTechnicalInspectionData[8]) ? CarMTechnicalInspectionData[8] : undefined
    }
  ];

  let MosFullInfo = {
    carPrice: await CarPrice,
    carMileage: await CarMileage,
    carMileageInfo: CarMileageInfo,
    carTechnicalInspection: CarTechnicalInspection
  };
  await ChangeTaskItemValue2(taskDB, id, vin, 'complete', 'mos', MosFullInfo);
}

async function CheckInNotaryBase (driver, id, vin, taskDB) {
  await ChangeTaskItemValue2(taskDB, id, vin, 'processing');
  await WaitWhileNotDisplayed(driver, NotarySearchInfoXpath, false);
  await ClickToObject(driver, NotarySearchInfoXpath, false);
  await WaitWhileNotDisplayed(driver, NotaryINFieldXpath, false);
  await SendValueToField(driver, NotaryINFieldXpath, vin, false);
  await ClickToObject(driver, NotarySubmitButtonXpath, false);

  await WaitWhileNotDisplayed(driver, NotaryCaptchaImageXpath, false);

  await SolveCaptcha(driver);

  await ChangeTaskItemValue2(taskDB, id, vin, 'waiting');
  await WaitWhileDisplayed(driver, NotaryLoadingSpinnerXpath, true, 'WAITING NOTARY RESULT', 'NOTARY RESULT IS VISIBLE');
  await ChangeTaskItemValue2(taskDB, id, vin, 'parsing');

  let federalNotaryChamber;
  let federalResource;
  try {
    await driver.wait(until.elementLocated(By.xpath(NotaryResultNotFoundXpath)), 2000).then(async () => {
      federalNotaryChamber = await driver.findElement(By.xpath(NotaryResultNotFoundXpath)).getText();
      await console.log('NOTARY ERROR TEXT: ', await federalNotaryChamber);
    });
    try {
      await ClickToObject(driver, NotarySecondResultTabXpath, false);
      await driver.wait(until.elementLocated(By.xpath(NotaryResultNotFoundXpath)), 2000).then(async () => {
        federalResource = await driver.findElement(By.xpath(NotaryResultNotFoundXpath)).getText();
        await console.log('NOTARY SECOND TAB ERROR TEXT: ', await federalResource);
      });
      let notaryData = {
        federalNotaryChamber: await federalNotaryChamber,
        federalResource: await federalResource
      };
      await ChangeTaskItemValue2(taskDB, id, vin, 'complete', 'notary', notaryData);
      return;
    } catch (e) {
      await console.log('SECOND TAB ERROR NOT FOUND, MAY BE RESULT TABLE IS VISIBLE');
    }
  } catch (e) {
    await console.log('ERROR NOT FOUND, MAY BE RESULT TABLE IS VISIBLE');
  }

  let registrationDate = await driver.findElement(By.xpath(`//*[contains(@class, 'table-search')]//*/td[2]`)).getText();
  let pledgeNumber = await driver.findElement(By.xpath(`//*[contains(@class, 'table-search')]//*/td[3]/a[@target='_blank']`)).getText();
  let pledgorData = await driver.findElement(By.xpath(`//*[contains(@class, 'table-search')]//*/td[5]`)).getText();
  let pledgeeData = await driver.findElement(By.xpath(`//*[contains(@class, 'table-search')]//*/td[6]`)).getText();

  await ClickToObject(driver, NotaryResultTablePledgorXpath, false);
  await WaitWhileNotDisplayed(driver, NotaryResultHeaderXpath, false);
  let getTablesCount = await GetElementsLength(driver, NotaryResultElementsCountXpath);
  await console.log('DATA LENGTH: ', await getTablesCount);

  let resultData = [];
  for (let ii = 0; ii < await getTablesCount; ii++) {
    let extraArray = [];
    let getTableLinesCount = GetElementsLength(driver, await NotaryResultTableDataLengthXpath(ii));
    for (let kk = 1; kk < await getTableLinesCount + 1; kk++) {
      let lineResult = await driver.findElement(By.xpath(await NotaryGetDataFromResultTableXpath(ii, kk))).getText();
      await extraArray.push(await lineResult);
    }
    await resultData.push(extraArray);
  }

  try {
    await ClickToObject(driver, NotarySecondResultTabXpath, false);
    await driver.wait(until.elementLocated(By.xpath(NotaryResultNotFoundXpath)), 2000).then(async () => {
      federalResource = await driver.findElement(By.xpath(NotaryResultNotFoundXpath)).getText();
      await console.log('NOTARY SECOND TAB ERROR TEXT: ', await federalResource);
    });
  } catch (e) {
    await console.log('SECOND TAB ERROR NOT FOUND, MAY BE RESULT TABLE IS VISIBLE');
  }

  await ClickToObject(driver, NotaryCloseResultTableXpath, false);
  await ClickToObject(driver, NotaryCheckChangesHistoryXpath, false);

  let pdfLinksCount = await GetElementsLength(driver, NotaryPDFLinkCountXpath);
  await console.log('PDF LINKS LENGTH: ', await pdfLinksCount);

  let endDate = await GetEndDate(driver, vin, await pdfLinksCount);
  await console.log(`END DATE = ${await endDate}`);

  await Sleep(1000);
  let notaryFullData = {
    federalNotaryChamber: await federalNotaryChamber,
    federalResource: await federalResource,
    registrationDate: await registrationDate,
    pledgeNumber: await pledgeNumber,
    pledgor: await pledgorData,
    pledgorData: resultData,
    pledgee: await pledgeeData,
    endDate: (!!endDate) ? endDate : 'VIN НОМЕР ВСЕ ЕЩЕ НАХОДИТСЯ СПИСКАХ ЛИЗИНГА'
  };

  await ChangeTaskItemValue2(taskDB, id, vin, 'complete', 'notary', notaryFullData);
}

async function RefreshCaptchaImage (driver) {
  await ClickToObject(driver, NotaryCancelCaptchaFillingXpath, false);
  await WaitWhileNotDisplayed(driver, NotaryCaptchaSubmitXpath, false);
  await ClickToObject(driver, NotaryCaptchaSubmitXpath, false);
  await driver.sleep(1000);
}

async function SolveCaptcha (driver) {
  let captchaText = await GetTextFromCaptcha(driver, NotaryCaptchaImageXpath);

  await SendValueToField(driver, NotaryCaptchaFieldXpath, captchaText, false);
  await ClickToObject(driver, NotaryCaptchaSubmitXpath, false);

  try {
    await driver.sleep(2500);
    if (!!await driver.findElement(By.xpath(NotaryLoadingSpinnerXpath)).isDisplayed()) {
      return;
    }
  } catch (e) {
    await console.log('CAPTCHA REFRESHING');
    await SolveCaptcha(driver);
  }
}

async function GetEndDate (driver, vin, linksCount) {
  let endDate = '';
  for (let ii = 0; ii < await linksCount; ii++) {
    let pdfLink = await driver.findElement(By.xpath(await NotaryGetPDFLinkXpath(ii + 1))).getAttribute('href');
    let pdfDate = await driver.findElement(By.xpath(await NotaryGetPDFLinkXpath(ii + 1))).getText();
    await console.log('CHECKING HREF: ', await pdfLink);
    await console.log('CHECKING DATE: ', await pdfDate);

    let destination;
    await new Promise(async (resolve, reject) => {
      await downloader.get(pdfLink, 'CHECKING-FILE', 'pdf', './');
      await downloader.on('done', async (dst) => {
        destination = await dst;
        await resolve(true);
      });
    });
    let noVIN = await GetInfoByVINFromPDF(await destination, vin);
    await console.log('NO VIN VALUE: ', await noVIN);
    if (!!noVIN) {
      endDate = await pdfDate;
      return await endDate;
    }
  }
}

async function GetInfoByVINFromPDF (pdfName, vin) {
  let noVIN = false;
  await Sleep(500);
  let dataBuffer = await fs.readFileSync(`./${await pdfName.trim()}`);
  await Sleep(500);
  await pdfParsing(await dataBuffer).then(async (data) => {
    await fs.writeFileSync(`./ParsedData.txt`, await data.text, {
      encoding: 'utf8',
      flag: 'w'
    });

    let parsedArray = await fs.readFileSync('./ParsedData.txt', 'utf8').split('\n');
    await console.log(`${await pdfName} IS PARSED`);

    let haveVIN = false;
    await console.log(`CHECKING VIN = ${await vin}`);
    for (let kkk = 0; kkk < await parsedArray.length; kkk++) {
      if (await parsedArray[kkk].includes(await vin)) {
        await console.log('VIN INCLUDES, VALUE: ', await parsedArray[kkk]);
        return haveVIN = true;
      }
    }
    if (!await haveVIN) {
      noVIN = true;
    }
  });

  return await noVIN;
}

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.route('/gibdd/get-info-about-auto').get(
  async function (request, response) {
    const { vin, type } = await request.body;
    let id = await CreateCollectionID();
    await ChangeTaskItemValue(taskDB, id, vin, 'starting');
    await console.log('TYPE: ', await type, 'VIN: ', await vin, ' ID: ', await id);
    (!!vin && type === 'history' || !!vin && type === 'dtp' || !!vin && type === 'wanted' || !!vin && type === 'restrictions' || !!vin && type === 'standard' || !!vin && type === 'customs' || !!vin && type === 'parsing' || !!vin && type === 'mos' || !!vin && type === 'notary')
      ? await response.status(200).send(await taskDB.get(`task${id}`))
      : await response.status(404).send('Request TYPE not correct or VIN is empty');

    let proxyData = await GetProxyData();
    let ProxyQueue = await GetProxyQueue();

    switch (await type) {
      case 'history':
        return await ProxyQueue.add(async () => {
          let driver = await CreateDriver(mainURL, 'chrome', proxyData.address, proxyData.login, proxyData.password);
          await GetInfoAboutAutoHistory(driver, id, vin, taskDB).then(async () => {
            await CheckOnOtherServices(driver, id, vin, taskDB, type).then(async () => {
              driver = await CreateDriver(mosURL, 'chrome', proxyData.address, proxyData.login, proxyData.password, true);
              await CheckVINOnMos(driver, id, vin, taskDB).then(async () => {
                await DestroyDriver(driver);
              });
            });
          });
        });
      case 'dtp':
        return await ProxyQueue.add(async () => {
          let driver = await CreateDriver(mainURL, 'chrome', proxyData.address, proxyData.login, proxyData.password);
          await GetInfoAboutAutoDTP(driver, id, vin).then(async () => {
            await DestroyDriver(driver);
          });
        });
      case 'wanted':
        return await ProxyQueue.add(async () => {
          let driver = await CreateDriver(mainURL, 'chrome', proxyData.address, proxyData.login, proxyData.password);
          await GetWantedStatus(driver, id, vin).then(async () => {
            await DestroyDriver(driver);
          });
        });
      case 'restrictions':
        return await ProxyQueue.add(async () => {
          let driver = await CreateDriver(mainURL, 'chrome', proxyData.address, proxyData.login, proxyData.password);
          await GetRestrictedStatus(driver, id, vin).then(async () => {
            await driver.get(notaryURL);
            await CheckInNotaryBase(driver, id, vin, taskDB).then(async () => {
              await DestroyDriver(driver);
            });
          });
        });
      case 'standard':
        return await GetInfoFromGosStandardQueue.add(async () => {
          let driver = await CreateDriver(gostURL, 'chrome');
          await CheckGosStandardInfoByVIN(driver, id, vin, taskDB).then(async () => {
            await DestroyDriver(driver);
          });
        });
      case 'customs':
        return await GetInfoFromCustomsQueue.add(async () => {
          let driver = await CreateDriver(customsURL, 'chrome');
          await CheckCustomsInfoVyVIN(driver, id, vin, taskDB).then(async () => {
            await DestroyDriver(driver);
          });
        });
      case 'mos':
        return await GetInfoFromCustomsQueue.add(async () => {
          let driver = await CreateDriver(mosURL, 'chrome', undefined, undefined, undefined, true);
          await CheckVINOnMos(driver, id, vin, taskDB).then(async () => {
            await DestroyDriver(driver);
          });
        });
      case 'notary':
        return await GetInfoFromCustomsQueue.add(async () => {
          let driver = await CreateDriver(notaryURL, 'chrome', proxyData.address, proxyData.login, proxyData.password);
          await CheckInNotaryBase(driver, id, vin, taskDB).then(async () => {
            await DestroyDriver(driver);
          });
        });
      case 'parsing':
        for (let pp = 0; pp < 3; pp++) {
          ParseVINNumbersFromFile(vins, pp);
          await Sleep(1000);
        }
        return;
      default:
        return console.log('TYPE NOT FOUND');
    }
  }
);

async function CheckOnOtherServices (driver, id, vin, taskDB, type) {
  let checkStatus = await taskDB.get(`task${id}.status`);
  if (await checkStatus === 'error') {
    await DestroyDriver(driver);
    await RecheckingErrorVIN(id, vin, taskDB, type);
  } else if (await checkStatus === 'waiting' || await checkStatus === 'complete') {
    await DestroyDriver(driver);
  }
  driver = await CreateDriver(customsURL, 'chrome');
  await CheckCustomsInfoVyVIN(driver, id, vin, taskDB).then(async () => {
    await driver.get(gostURL);
    await CheckGosStandardInfoByVIN(driver, id, vin, taskDB).then(async () => {
      await DestroyDriver(driver);
    });
  });
}

app.route('/test/get-auto-types').get(
  async function (request, response) {
    await response.status(200).send(await tsTypeDB.get(`type`));
  }
);

app.route('/test/get-ids-array').get(
  async function (request, response) {
    await response.status(200).send(await vinTaskDB.get(`idsBox`));
  }
);

app.route('/test/get-data-from-ids-array').get(
  async function (request, response) {
    const { ids } = await request.body;
    let data = [];

    for (let i = 0; i < ids.length; i++) {
      await data.push(await vinTaskDB.get(`task${ids[i]}`));
    }

    await response.status(200).send(await data);
  }
);

app.route('/test/get-error-vins').get(async (request, response) => {
  let data = await GetErrorVIN();
  await response.status(200).send(await data);
});

app.route('/gibdd/info-collection-status').get(
  async function (request, response) {
    await CheckStatusQueue.add(async () => {
      const { id } = await request.body;
      await response.status(200).send(await taskDB.get(`task${id}`));
    });
  }
);

app.route('/test/without-proxy').post(async function () {
  CreateDriver(mosURL).then(async () => {
    await console.log('PAGE DOWNLOADED');
  });
});

async function CreateDriver (url, browser = 'chrome', address, login, password, useCookie) {
  if (!!address && !!login && !!password) {
    await console.log('PROXY ADDRESS: ', await address);
    await console.log('PROXY LOGIN: ', await login);
    await console.log('PROXY PASSWORD: ', await password);
  }

  let options = await new chrome.Options();

  if (!!useCookie) {
    await options.addArguments('--user-data-dir=./Profilies/');
    await options.addArguments('--profile-directory=Profile 3');
  }

  // await options.addArguments('--window-size=1280,960');
  // await options.addArguments('--window-position=490,90');

  await options.addArguments('--headless');

  await options.addArguments('--ignore-certificate-errors');
  await options.addArguments('--disable-dev-shm-usage');
  await options.addArguments('--no-sandbox');
  // await options.addArguments('--enable-logging');

  await options.addArguments('--disable-gpu');
  await options.addArguments('--start-maximized');
  await options.addArguments('--disable-popup-blocking');
  await options.addArguments('--ignore-certificate-errors');

  let driver = (!!address && !!login && !!password)
    ? await new webdriver.Builder().forBrowser(browser).setChromeOptions(options).withCapabilities(webdriver.Capabilities.chrome()).setProxy(proxy.socks(address, login, password)).build()
    : await new webdriver.Builder().forBrowser(browser).setChromeOptions(options).build();
  await console.log('Driver Was Created');

  await driver.get(url);

  return await driver;
}

function DestroyDriver (driver) {
  driver.quit();
  driver = undefined;
  console.log('Driver is destroyed');
}

app.listen(port, function () {
  console.log(appName + ' started on ' + port + ' port');
  solver.setApiKey('a97c45924a32fa052f3b6ea056de20b1');
});
